from django.db import models


class Quiz(models.Model):
    name = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    creator = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    description = models.TextField()

    class Meta:
        verbose_name_plural = 'Quizzes'

    def __str__(self):
        return self.name


class Question(models.Model):
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name='questions')
    text = models.CharField(max_length=200)

    def __str__(self):
        return self.text


class QuestionChoice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='choices')
    text = models.CharField(max_length=200)
    is_correct = models.BooleanField(default=False)

    def __str__(self):
        return self.text


class UserQuiz(models.Model):
    STATE_CREATED = 'CR'
    STATE_INVITED = 'IN'
    STATE_STARTED = 'ST'
    STATE_FINISHED = 'FN'
    STATE_CANCELLED = 'CN'

    STATE_CHOICES = (
        (STATE_CREATED, 'Created'),
        (STATE_INVITED, 'Invited'),
        (STATE_STARTED, 'Started'),
        (STATE_FINISHED, 'Finished'),
        (STATE_CANCELLED, 'Cancelled'),
    )

    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    quiz = models.ForeignKey('Quiz', on_delete=models.CASCADE, related_name='user_quizzes')
    created_at = models.DateTimeField(auto_now_add=True)
    cancelled_at = models.DateTimeField(null=True, blank=True)
    finished_at = models.DateTimeField(null=True, blank=True)
    score = models.IntegerField(default=0)
    state = models.CharField(max_length=2, choices=STATE_CHOICES, default=STATE_CREATED)

    class Meta:
        verbose_name_plural = 'User Quizzes'
        unique_together = ('user', 'quiz')

    def __str__(self):
        return '{} - {}'.format(self.user, self.quiz)

    def update_score(self):
        correct_responses = self.responses.filter(answer__is_correct=True).count()
        correct_percentage = correct_responses / self.responses.count() * 100
        self.score = correct_percentage
        self.save()

    def invite(self):
        self.state = self.STATE_INVITED
        self.save()

    def start(self):
        self.state = self.STATE_STARTED
        self.save()

    def finish(self):
        self.state = self.STATE_FINISHED
        self.save()

    def cancel(self):
        self.state = self.STATE_CANCELLED
        self.save()


class UserQuizResponse(models.Model):
    user_quiz = models.ForeignKey('UserQuiz', on_delete=models.CASCADE, related_name='responses')
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    answered_at = models.DateTimeField(null=True, blank=True)
    answer = models.ForeignKey('QuestionChoice', on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'User Quiz Responses'
        unique_together = ('user_quiz', 'question')

    def __str__(self):
        return '{} - {}'.format(self.user_quiz, self.question)
