from rest_framework.routers import DefaultRouter

from . import views


router = DefaultRouter()
router.register(r'quiz', views.QuizViewSet)
# register QuestionViewSet with the router after /quiz/<pk>/
router.register(r'quiz/(?P<quiz_pk>\d+)/questions', views.QuestionViewSet)
router.register(r'quiz/(?P<quiz_pk>\d+)/questions/(?P<question_pk>\d+)/choices', views.QuestionChoiceViewSet)
router.register(r'userquizzes', views.UserQuizViewSet)
router.register(r'userquizzes/(?P<userquiz_pk>\d+)/responses', views.UserQuizResponseViewSet)

urlpatterns = router.urls
