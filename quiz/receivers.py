from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import UserQuiz, UserQuizResponse
from notifications.emails.quiz_invitation import InvitationEmail
from notifications.emails.quiz_completed import CompletedEmail


@receiver(post_save, sender=UserQuiz)
def handle_user_quiz_save(sender, instance, created, **kwargs):
    """
    Send an invitation email to the user who was invited to a quiz.
    """
    if created:
        InvitationEmail(user=instance.user, quiz=instance.quiz).send()
        instance.invite()


@receiver(post_save, sender=UserQuizResponse)
def handle_user_response_save(sender, instance, created, **kwargs):
    """
    Send an invitation email to the user who was invited to a quiz.
    """

    # Update the score
    user_quiz = instance.user_quiz
    user_quiz.update_score()

    if created:
        if user_quiz.state in [UserQuiz.STATE_INVITED, UserQuiz.STATE_CREATED]:
            # The participant is starting the quiz.
            user_quiz.start()

        if user_quiz.quiz.questions.count() == user_quiz.responses.count():
            # The participant has completed the quiz.
            CompletedEmail(user=user_quiz.user, quiz=user_quiz.quiz, score=user_quiz.score).send()
            user_quiz.finish()
