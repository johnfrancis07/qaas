from rest_framework import serializers

from quiz.models import Quiz, Question, QuestionChoice, UserQuiz, UserQuizResponse


class QuestionChoiceWithoutAnswerSerializer(serializers.ModelSerializer):
    """
    Serializer for QuestionChoice model without is_correct field.
    """
    class Meta:
        model = QuestionChoice
        fields = ('id', 'text')


class QuestionChoiceSerializer(serializers.ModelSerializer):
    """
    Serializer for QuestionChoice model.
    """
    class Meta:
        model = QuestionChoice
        fields = ('id', 'text', 'is_correct')


class QuestionSerializer(serializers.ModelSerializer):
    """
    Serializer for Question model.
    """
    choices = serializers.SerializerMethodField()

    def get_choices(self, obj):
        """
        Only show answers to the quiz questions if the user created the quiz.
        """
        if self.context['request'].user == obj.quiz.creator:
            return QuestionChoiceSerializer(obj.choices.all(), many=True, context=self.context).data
        else:
            return QuestionChoiceWithoutAnswerSerializer(obj.choices.all(), many=True, context=self.context).data

    class Meta:
        model = Question
        fields = ('id', 'text', 'choices')


class QuizSerializer(serializers.ModelSerializer):
    """
    Serializer to retrieve a list of Quiz objects and create a new Quiz object.
    """
    class Meta:
        model = Quiz
        fields = ('id', 'name', 'description')

    def create(self, validated_data):
        """
        For added convenience, we allow questions and question choices to be added to a Quiz on creation.
        """
        questions_data = self.context['request'].data.get('questions', [])
        quiz = Quiz.objects.create(creator=self.context['request'].user, **validated_data)
        for question_data in questions_data:
            choices_data = question_data.get('choices', [])
            question = Question.objects.create(quiz=quiz, text=question_data['question'])
            for choice_data in choices_data:
                QuestionChoice.objects.create(question=question, text=choice_data['choice'],
                                              is_correct=choice_data.get('is_correct', False))
        return quiz


class QuizDetailSerializer(serializers.ModelSerializer):
    """
    Serializer to retrieve, update and delete a Quiz object.
    """
    questions = QuestionSerializer(many=True, read_only=True)

    class Meta:
        model = Quiz
        fields = ('id', 'name', 'creator', 'created_at', 'description', 'questions')

    def to_representation(self, instance):
        # Override to_representation to include questions and user_quizzes
        representation = super().to_representation(instance)
        representation['participants'] = [
            {
                "id": user_quiz.user.id,
                "name": user_quiz.user.first_name + ' ' + user_quiz.user.last_name,
                "state": user_quiz.state,
                "score": user_quiz.score,
                "finished_at": user_quiz.finished_at,
                "responses": UserQuizResponseSerializer(user_quiz.responses.all(), many=True).data
            }
            for user_quiz in instance.user_quizzes.all()
        ]

        return representation


class UserQuizResponseSerializer(serializers.ModelSerializer):
    """
    Serializer for the UserQuizResponse model.
    """
    class Meta:
        model = UserQuizResponse
        fields = ('id', 'question', 'answer')
        list_display = ('answered_at',)

    def validate(self, data):
        user_quiz = self.context['user_quiz']
        # Check that the question belongs to the quiz
        if data['question'] not in user_quiz.quiz.questions.all():
            raise serializers.ValidationError('This question does not belong to this quiz.')

        # Check that the response is valid
        if data['answer'] not in data['question'].choices.all():
            raise serializers.ValidationError('This answer is not valid for this question.')

        data['user_quiz'] = user_quiz

        return data


class UserQuizSerializer(serializers.ModelSerializer):
    """
    Serializer to retrieve a list of UserQuiz objects and create a new UserQuiz object.
    """
    class Meta:
        model = UserQuiz
        fields = ('id', 'user', 'quiz', 'state')
        read_only_fields = ('state',)
        extra_kwargs = {
            'user': {'write_only': True},
        }

    def create(self, validated_data):
        user_quiz = UserQuiz.objects.create(**validated_data)
        return user_quiz


class UserQuizDetailSerializer(serializers.ModelSerializer):
    """
    Serializer to retrieve, update and delete a UserQuiz object.
    """
    responses = UserQuizResponseSerializer(many=True, read_only=True)

    class Meta:
        model = UserQuiz
        fields = ('id', 'quiz', 'responses')
        read_only_fields = ('state',)

    def to_representation(self, instance):
        # Override to_representation to include quiz
        representation = super().to_representation(instance)
        representation['quiz'] = {
            "id": instance.quiz.id,
            "name": instance.quiz.name,
            "description": instance.quiz.description,
            "questions": QuestionSerializer(instance.quiz.questions.all(), context=self.context, many=True).data
        }
        return representation
