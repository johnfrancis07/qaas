from rest_framework.test import APITestCase
from rest_framework.test import APIClient
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User


class QuizTest(APITestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_superuser('user', 'user@user.com', 'user23')
        self.token = Token.objects.create(user=self.user)

    def test_authenticated_create_quiz(self):
        self.client.force_login(user=self.user)
        quiz_data = {'name': 'Test Quiz', 'description': 'Test Quiz Description'}
        response = self.client.post(
            '/api/quiz/', data=quiz_data, format='json', HTTP_AUTHORIZATION='Token ' + self.token.key)
        self.assertEqual(response.status_code, 201)

    def test_unauthenticated_create_quiz(self):
        quiz_data = {'name': 'Test Quiz', 'description': 'Test Quiz Description'}
        response = self.client.post(
            '/api/quiz/', data=quiz_data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_authenticated_create_quiz_with_invalid_data(self):
        self.client.force_login(user=self.user)
        quiz_data = {'title': 'Test Quiz', 'id': 1}
        response = self.client.post(
            '/api/quiz/', data=quiz_data, format='json', HTTP_AUTHORIZATION='Token ' + self.token.key)
        self.assertEqual(response.status_code, 400)

    def test_authenticated_create_quiz_with_questions(self):
        self.client.force_login(user=self.user)
        quiz_data = {
            "name": "Quiz title",
            "description": "Quiz description",
            "creator": 1,
            "questions": [
                {
                    "question": "What colour is the sky on a clear day?",
                    "choices": [
                        {
                            "choice": "Blue",
                            "is_correct": True
                        },
                        {
                            "choice": "Orange"
                        },
                        {
                            "choice": "Gray"
                        }
                    ]
                }
            ]
        }
        response = self.client.post(
            '/api/quiz/', data=quiz_data, format='json', HTTP_AUTHORIZATION='Token ' + self.token.key)
        self.assertEqual(response.status_code, 201)

        detail_response = self.client.get('/api/quiz/{}/'.format(response.data['id']), format='json',
                                          HTTP_AUTHORIZATION='Token ' + self.token.key)
        self.assertTrue(detail_response.data['questions'][0]['choices'][0]['is_correct'])
