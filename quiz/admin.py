import csv

from django.contrib import admin
from django.core import serializers
from django.http import HttpResponse
from django.utils.safestring import mark_safe
from django.urls import reverse
from rest_framework_tracking.admin import APIRequestLogAdmin
from rest_framework_tracking.models import APIRequestLog

from .models import Quiz, Question, QuestionChoice, UserQuiz, UserQuizResponse


class EditLinkMixin(object):
    @staticmethod
    def edit_link(instance):
        url = reverse(
            "admin:{}_{}_change".format(instance._meta.app_label,  instance._meta.model_name),
            args=[instance.pk]
        )
        if instance.pk:
            return mark_safe(u'<a href="{}">Edit</a>'.format(url))
        else:
            return ''


class QuestionAdminInline(EditLinkMixin, admin.TabularInline):
    model = Question
    readonly_fields = ('edit_link',)


class QuestionChoiceAdminInline(admin.TabularInline):
    model = QuestionChoice


class QuizAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_at'
    list_display = ('name', 'description', 'created_at', 'creator')
    inlines = [QuestionAdminInline]

    def get_queryset(self, request):
        qs = super(QuizAdmin, self).get_queryset(request)
        return qs.select_related('creator')


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('quiz', 'text')
    inlines = [QuestionChoiceAdminInline]

    def get_queryset(self, request):
        return super(QuestionAdmin, self).get_queryset(request).select_related('quiz')


class UserQuizResponseInline(admin.TabularInline):
    model = UserQuizResponse


class UserQuizAdmin(admin.ModelAdmin):
    list_display = ('user', 'quiz', 'created_at', 'state', 'score')
    list_filter = ('quiz',)
    inlines = [UserQuizResponseInline]

    def get_queryset(self, request):
        return super(UserQuizAdmin, self).get_queryset(request).select_related('user', 'quiz')


class UserQuizResponseAdmin(admin.ModelAdmin):
    list_display = ('user_quiz', 'question', 'answer', 'answered_at')
    list_filter = ('question',)

    def get_queryset(self, request):
        return super(UserQuizResponseAdmin, self).get_queryset(request).select_related('user_quiz', 'question')


class CustomTrackingAdmin(APIRequestLogAdmin):
    actions = ['download_csv', 'download_json']

    def download_csv(self, request, queryset):
        meta = self.model._meta
        field_names = [field.name for field in meta.fields]
        field_names.remove('response')  # remove response field - probably not super useful for csv metrics

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=qaas_quiz_metrics.csv'
        writer = csv.writer(response)

        writer.writerow(field_names)
        for obj in queryset:
            writer.writerow([getattr(obj, field) for field in field_names])

        return response

    def download_json(self, request, queryset):
        response = HttpResponse(content_type='application/json')
        response['Content-Disposition'] = 'attachment; filename=qaas_quiz_metrics.json'

        response.write(serializers.serialize('json', queryset))

        return response

    download_csv.short_description = "Download Selected as CSV"
    download_json.short_description = "Download Selected as JSON"


# Re-register Metrics Admin with additional download actions
admin.site.unregister(APIRequestLog)
admin.site.register(APIRequestLog, CustomTrackingAdmin)


admin.site.register(Quiz, QuizAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(UserQuiz, UserQuizAdmin)
admin.site.register(UserQuizResponse, UserQuizResponseAdmin)
