from rest_framework import viewsets, permissions
from rest_framework_tracking.mixins import LoggingMixin

from quiz.models import Quiz, Question, QuestionChoice, UserQuiz, UserQuizResponse
from quiz.serializers import QuizSerializer, QuizDetailSerializer, UserQuizSerializer, UserQuizResponseSerializer, \
    QuestionSerializer, QuestionChoiceSerializer, UserQuizDetailSerializer


class QuizViewSet(LoggingMixin, viewsets.ModelViewSet):
    """
    API endpoint that allows quizzes to be viewed or edited.
    """
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Quiz.objects.all().order_by('-created_at')
    serializer_class = QuizSerializer
    detail_serializer_class = QuizDetailSerializer

    def get_serializer_class(self):
        """
        Return the serializer class based on the request.
        """
        if self.action == 'retrieve':
            return self.detail_serializer_class
        return self.serializer_class

    def get_queryset(self):
        """
        Fetch a list of all quizzes for the current user.
        """
        return Quiz.objects.filter(creator=self.request.user)


class QuestionViewSet(LoggingMixin, viewsets.ModelViewSet):
    """
    API endpoint that allows questions to be viewed or edited.
    """
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer

    def perform_create(self, serializer):
        """
        Create a new question for the current user.
        """
        quiz_pk = self.kwargs['quiz_pk']
        quiz = Quiz.objects.get(pk=quiz_pk, creator=self.request.user)
        if not quiz:
            raise Exception('Quiz not found.')

        serializer.save(quiz=quiz)

    def get_queryset(self, *args, **kwargs):
        """
        Fetch a list of all questions for the current user.
        """
        quiz_pk = self.kwargs['quiz_pk']
        quiz = Quiz.objects.get(pk=quiz_pk, creator=self.request.user)
        if not quiz:
            return Question.objects.none()

        return self.queryset.filter(quiz=quiz)


class QuestionChoiceViewSet(LoggingMixin, viewsets.ModelViewSet):
    """
    API endpoint that allows question choices to be viewed or edited.
    """
    permission_classes = (permissions.IsAuthenticated,)
    queryset = QuestionChoice.objects.all()
    serializer_class = QuestionChoiceSerializer

    def perform_create(self, serializer):
        """
        Create a new question choice for the current user.
        """
        question_pk = self.kwargs['question_pk']
        question = Question.objects.get(pk=question_pk, quiz__creator=self.request.user)
        if not question:
            raise Exception('Question not found.')

        serializer.save(question=question)

    def get_queryset(self, *args, **kwargs):
        """
        Fetch a list of all question choices for the current user.
        """
        question_pk = self.kwargs['question_pk']
        question = Question.objects.get(pk=question_pk, quiz__creator=self.request.user)
        if not question:
            return QuestionChoice.objects.none()

        return self.queryset.filter(question=question)


class UserQuizViewSet(LoggingMixin, viewsets.ModelViewSet):
    """
    API endpoint that allows users to start and finish quizzes.
    """
    permission_classes = (permissions.IsAuthenticated,)
    queryset = UserQuiz.objects.all().order_by('-created_at')
    serializer_class = UserQuizSerializer
    detail_serializer_class = UserQuizDetailSerializer

    def get_serializer_class(self):
        """
        Return the serializer class based on the request.
        """
        if self.action == 'retrieve':
            return self.detail_serializer_class
        return self.serializer_class

    def get_queryset(self):
        """
        Fetch a list of all quizzes for the current user.
        """
        return UserQuiz.objects.filter(user=self.request.user)


class UserQuizResponseViewSet(LoggingMixin, viewsets.ModelViewSet):
    """
    API endpoint that allows users to start and finish quizzes.
    """
    permission_classes = (permissions.IsAuthenticated,)
    queryset = UserQuizResponse.objects.all().order_by('-answered_at')
    serializer_class = UserQuizResponseSerializer

    def get_serializer_context(self):
        user_quiz = UserQuiz.objects.get(pk=self.kwargs['userquiz_pk'])
        return {'user_quiz': user_quiz}

    def get_queryset(self):
        """
        Fetch a list of all quizzes for the current user quiz instance.
        """
        return UserQuizResponse.objects.filter(user_quiz__pk=self.kwargs['userquiz_pk'])
