from django.contrib import admin

# Register your models here.
from notifications.models import Email


class EmailAdmin(admin.ModelAdmin):
    list_display = ('id', 'created_at', 'updated_at', 'from_email', 'to_email', 'subject', 'plaintext')


admin.site.register(Email, EmailAdmin)
