from django.db import models


class Email(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    from_email = models.EmailField(max_length=254)
    to_email = models.EmailField(max_length=254)
    subject = models.CharField(max_length=254)
    plaintext = models.TextField()
    html = models.TextField()

    def __str__(self):
        return "Email sent from {self.from_email} to {self.to_email}".format(self=self)