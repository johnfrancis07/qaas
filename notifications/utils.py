import smtplib
import logging

from django.conf import settings
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

logger = logging.getLogger(__name__)


def send_mail(email):
    """ Initiate the sending of an email by the configured method

    Args:
        email (notifications.models.Email): Object containing the email to be sent
    """

    msg = MIMEMultipart('alternative')
    msg['From'] = email.from_email
    msg['To'] = email.to_email
    msg['Subject'] = email.subject
    txt_version = MIMEText(email.plaintext, 'plain')
    msg.attach(txt_version)

    html_version = MIMEText(email.html, 'html')
    msg.attach(html_version)
    msg_str = msg.as_string()

    server = smtplib.SMTP(settings.EMAIL_HOST, settings.EMAIL_PORT)
    server.ehlo()
    server.starttls()
    server.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)

    try:
        server.sendmail(email.from_email, email.to_email, msg_str)
        logger.info("Email sent to {recipient}".format(recipient=email.to_email))
    except smtplib.SMTPException:
        logger.exception("Error sending email", exc_info=True)

    server.quit()
