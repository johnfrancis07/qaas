from abc import abstractmethod

from django.conf import settings
from django.template import loader as template_loader
from django.utils.html import strip_tags

from notifications.models import Email
from notifications.utils import send_mail


class BaseEmail(object):
    """ Base class to handle email creation, rendering and sending initialization

        When extending this class for the purposes of creating a new email the following should be implemented:
          - template: html template to render and send
          - get_context_data: context to pass to template
          - get_email_kwargs: return dict containing at least the subject and recipient (keys: "subject", "to_email")

        Attributes:
            template (str): relative path to html template
    """

    template = None

    @abstractmethod
    def get_context_data(self):
        """Should return context used to render email html"""
        raise NotImplementedError

    @abstractmethod
    def get_email_kwargs(self):
        """Should return kwargs used to build notifications.models.Email object"""
        raise NotImplementedError

    def get_html(self):
        """Render html template to formatted string
        """
        context = self.get_context_data()
        return template_loader.render_to_string(self.template, context)

    def send(self):
        """ Initializes the sending of an email """

        email_kwargs = self.get_email_kwargs()
        email_kwargs["html"] = self.get_html()

        # Update email_kwargs with missing defaults if required
        if "from_email" not in email_kwargs:
            email_kwargs["from_email"] = settings.FROM_EMAIL

        if "plaintext" not in email_kwargs:
            email_kwargs["plaintext"] = strip_tags(email_kwargs["html"])

        email = Email.objects.create(**email_kwargs)
        send_mail(email)
        return email
