from django.conf import settings

from .base import BaseEmail


SUBJECT = "Your quiz has been completed. Here are your results."


class CompletedEmail(BaseEmail):
    template = "quiz_completed.html"

    def __init__(self, user, quiz, score):
        self.user = user
        self.quiz = quiz
        self.score = score

    def get_context_data(self):
        return {
            "recipient_name": self.user.first_name,
            "quiz_name": self.quiz.name,
            "score": self.score,
        }

    def get_email_kwargs(self):
        return {
            "from_email": settings.DEFAULT_FROM_EMAIL,
            "to_email": self.user.email,
            "subject": SUBJECT,
        }
