from django.conf import settings

from .base import BaseEmail


SUBJECT = "You've been invited to complete a quiz"


class InvitationEmail(BaseEmail):
    template = "quiz_invitation.html"

    def __init__(self, user, quiz):
        self.user = user
        self.quiz = quiz

    def get_context_data(self):
        return {
            "recipient_name": self.user.first_name,
            "quiz_name": self.quiz.name,
            "cta_link": "<< TODO: CTA link to dedicated frontend application >>",
        }

    def get_email_kwargs(self):
        return {
            "from_email": settings.DEFAULT_FROM_EMAIL,
            "to_email": self.user.email,
            "subject": SUBJECT,
        }
