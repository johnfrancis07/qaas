# QaaS-Backend

This is the backend service to the QaaS Web Application which provides backend functionality, 
a REST API for consumption by the frontend and the persistence of data.

[TOC]

## Installation

#### 1. Clone the repository
`git clone git@bitbucket.org:johnfrancis07/qaas.git`
#### 2. Change directory to the repository
`cd qaas`
#### 2. Setup a virtual python3 environment
For example with virtualenv:
`virtualenv -p python3 venv`
#### 3. Activate the virtual environment
`. venv/bin/activate`
#### 4. Install the dependencies
`pip install -r requirements.txt`
#### 5. Run migrations
`python manage.py migrate`
#### 6. Start the development server
`python manage.py runserver`


## Authentication
Authentication is handled by django rest framework Token Authentication.

#### To register a new user:
```
curl -X POST -H "Content-Type: application/json" -d '{"email": "john@example.com", "password": "pa$$word123", "first_name": "John", "last_name": "Smith"}' http://localhost:8000/auth/register/
```

#### To retrive an access token:
```
curl -X POST -H "Content-Type: application/json" -d '{"username": "john@example.com", "password": "pa$$word123"}' http://localhost:8000/auth/api-token/
```

To make an authenticated API request, pass the access token in the request Authorization header:

`curl -H "Authorization: Token <token>" http://localhost:8000/api/ ...`

## API

### Quizzes

#### List all quizzes

Retrieves a list of all quizzes belonging to the user (the creator).

* **GET** `/api/quiz/`
* **Parameters** None
* **Response** 200 OK, list of quizzes
* **Example** `curl -H "Authorization:Token <token>" http://localhost:8000/api/quiz/`
* **Browsable API** `http://localhost:8000/api/quiz/`

#### Create a new quiz

* **POST** `/api/quiz/`
* **Parameters** `{"name": "Quiz Title", "description": "Quiz Description"}`

You can also pass a list of questions and choices directly in the request payload.
```
{
   "name":"Quiz Title",
   "description":"Quiz Description",
   "questions":[
      {
         "question":"Question 1",
         "choices":[
            {"choice":"Choice 1"},
            {"choice":"Choice 2", "is_correct": true},
            {"choice":"Choice 3"}
         ]
      }
   ]
}
```

* **Response** 201 CREATED, new quiz
* **Example** `curl -X POST -H "Authorization:Token <token>" -H "Content-Type: application/json" -d '{"name": "Quiz Title", "description": "Quiz Description"}' http://localhost:8000/api/quiz/`
* **Browsable API** `http://localhost:8000/api/quiz/`

#### Retrieve a quiz

Retrieves a detailed quiz by id **along with all questions, choices, participants, statuses, responses, scores etc.**.

* **GET** `/api/quiz/<id>/`
* **Parameters** None
* **Response** 200 OK, quiz
* **Example** `curl -H "Authorization:Token <token>" http://localhost:8000/api/quiz/<id>/`
* **Browsable API** `http://localhost:8000/api/quiz/<id>/`

#### Update a quiz

* **PUT** `/api/quiz/<id>/`
* **Parameters** `{"name": "Quiz Title", "description": "Quiz Description"}`
* **Response** 200 OK, updated quiz
* **Example** `curl -X PUT -H "Authorization:Token <token>" -H "Content-Type: application/json" -d '{"name": "Quiz Title", "description": "Quiz Description"}' http://localhost:8000/api/quiz/<id>/`
* **Browsable API** `http://localhost:8000/api/quiz/<id>/`

#### Delete a quiz

* **DELETE** `/api/quiz/<id>/`
* **Parameters** None
* **Response** 204 NO CONTENT, quiz deleted
* **Example** `curl -X DELETE -H "Authorization:Token <token>" http://localhost:8000/api/quiz/<id>/`
* **Browsable API** `http://localhost:8000/api/quiz/<id>/`

### Quiz questions

#### Add a question to a quiz

* **POST** `/api/quiz/<id>/question/`
* **Parameters** `{"text": "Question 1"}`
* **Response** 201 CREATED, new question
* **Example** `curl -X POST -H "Authorization:Token <token>" -H "Content-Type: application/json" -d '{"text": "Question 1"}' http://localhost:8000/api/quiz/<id>/question/`
* **Browsable API** `http://localhost:8000/api/quiz/<id>/question/`

#### Remove a question from a quiz

* **DELETE** `/api/quiz/<id>/question/<question_id>/`
* **Parameters** None
* **Response** 204 NO CONTENT, question deleted
* **Example** `curl -X DELETE -H "Authorization:Token <token>" http://localhost:8000/api/quiz/<id>/question/<question_id>/`
* **Browsable API** `http://localhost:8000/api/quiz/<id>/question/<question_id>/`

#### Retrieve a question from a quiz

* **GET** `/api/quiz/<id>/question/<question_id>/`
* **Parameters** None
* **Response** 200 OK, question
* **Example** `curl -H "Authorization:Token <token>" http://localhost:8000/api/quiz/<id>/question/<question_id>/`
* **Browsable API** `http://localhost:8000/api/quiz/<id>/question/<question_id>/`

### Quiz question Choices

#### Add a choice to a question

* **POST** `/api/quiz/<id>/question/<question_id>/choices/`
* **Parameters** `{"text": "Choice 1", "is_correct": true}`
* **Response** 201 CREATED, new choice
* **Example** `curl -X POST -H "Authorization:Token <token>" -H "Content-Type: application/json" -d '{"text": "Choice 1", "is_correct": true}' http://localhost:8000/api/quiz/<id>/question/<question_id>/choices/`
* **Browsable API** `http://localhost:8000/api/quiz/<id>/question/<question_id>/choices/`

#### Update a choice

* **PUT** `/api/quiz/<id>/question/<question_id>/choices/<choice_id>/`
* **Parameters** `{"text": "Choice 1", "is_correct": true}`
* **Response** 200 OK, updated choice
* **Example** `curl -X PUT -H "Authorization:Token <token>" -H "Content-Type: application/json" -d '{"text": "Choice 1", "is_correct": true}' http://localhost:8000/api/quiz/<id>/question/<question_id>/choices/<choice_id>/`
* **Browsable API** `http://localhost:8000/api/quiz/<id>/question/<question_id>/choices/<choice_id>/`

#### Remove a choice from a question

* **DELETE** `/api/quiz/<id>/question/<question_id>/choices/<choice_id>/`
* **Parameters** None
* **Response** 204 NO CONTENT, choice deleted
* **Example** `curl -X DELETE -H "Authorization:Token <token>" http://localhost:8000/api/quiz/<id>/question/<question_id>/choices/<choice_id>/`
* **Browsable API** `http://localhost:8000/api/quiz/<id>/question/<question_id>/choices/<choice_id>/`

### Invite a participant to a quiz

* **POST** `/api/userquizzes/`
* **Parameters** `{"user": <user_id>, "quiz": <quiz_id>}`
* **Response** 201 CREATED, new userquiz
* **Example** `curl -X POST -H "Authorization:Token <token>" -H "Content-Type: application/json" -d '{"user": <user_id>, "quiz": <quiz_id>}' http://localhost:8000/api/userquizzes/`
* **Browsable API** `http://localhost:8000/api/userquizzes/`

#### Retrieve all of my quizzes as a participant 

* **GET** `/api/userquizzes/`
* **Parameters** None
* **Response** 200 OK, userquizzes
* **Example** `curl -H "Authorization:Token <token>" http://localhost:8000/api/userquizzes/`
* **Browsable API** `http://localhost:8000/api/userquizzes/`

#### Retrieve a quiz for a participant

This endpoint returns all questions, choices, and responses for this quiz.

* **GET** `/api/userquizzes/<id>/`
* **Parameters** None
* **Response** 200 OK, userquiz
* **Example** `curl -H "Authorization:Token <token>" http://localhost:8000/api/userquizzes/<id>/`
* **Browsable API** `http://localhost:8000/api/userquizzes/<id>/`

#### Create a question response

* **POST** `/api/userquizzes/<id>/responses/`
* **Parameters** `{"question": <question_id>, "answer": <choice_id>}`
* **Response** 201 CREATED, new response
* **Example** `curl -X POST -H "Authorization:Token <token>" -H "Content-Type: application/json" -d '{"question": <question_id>, "answer": <choice_id>}' http://localhost:8000/api/userquizzes/<id>/responses/`
* **Browsable API** `http://localhost:8000/api/userquizzes/<id>/responses/`

## Notifications

* When a participant is invited to a quiz, an invitation email automatically is sent to the participant.
* When a participant finishes a quiz, an email will automatically be sent to the participant with their results.

## Metrics

API requests are logged to the database and can be viewed in Django Admin.
To download, select the desired range (e.g today, last 7 days etc.) and select Download as CSV or JSON.

## Testing

#### To run unit tests:
`python manage.py test`

## Footnotes

* The browsable API allows you to make API requests directly in your browser and is available at `http://localhost:8000/api/`. Make sure you are logged in to make authenticated requests.
* Email credentials are stored in plaintext in settings.py. This should be moved out of version control for example to a server config file or environment variable.