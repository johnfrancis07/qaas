from rest_framework.test import APITestCase
from rest_framework.test import APIClient
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User


class QuizTest(APITestCase):

    def setUp(self):
        self.client = APIClient()

    def test_create_user(self):
        user_data = {
            'email': 'testuser@example.com',
            'first_name': 'Test',
            'last_name': 'User',
            'password': 'testpassword'
        }
        response = self.client.post(
            '/auth/register/', data=user_data, format='json')

        self.assertEqual(response.status_code, 201)
        self.assertEqual(User.objects.count(), 1)

    def test_retrieve_token(self):
        # Create User
        user_data = {
            'email': 'testuser@example.com',
            'first_name': 'Test',
            'last_name': 'User',
            'password': 'testpassword'
        }
        self.client.post(
            '/auth/register/', data=user_data, format='json')

        # Retrieve Token
        user_data = {
            'username': 'testuser@example.com',
            'password': 'testpassword'
        }
        response = self.client.post(
            '/auth/api-token/', data=user_data, format='json')

        user = User.objects.first()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['token'], Token.objects.get(user=user).key)

    def test_retrieve_token_invalid_credentials(self):
        user_data = {
            'username': 'invalid@example.com',
            'password': 'invalidpassword'
        }

        response = self.client.post(
            '/auth/api-token/', data=user_data, format='json')

        self.assertEqual(response.status_code, 400)

